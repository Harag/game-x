(in-package :common-lisp-user)

(defpackage :game-x
  (:use :cl :sdl2)
  (:shadow
   :sym-value
   :mod-value
   :set-render-draw-color
   )
  (:export
   #:game
   #:window
   #:renderer
   #:key-event
   #:key-sym
   #:scan-code
   #:sym-value
   #:mod-value
   #:x
   #:y
   #:xrel
   #:yrel
   #:state
   #:event-handler
   #:handle-events
 #:set-render-draw-color
   #:play))
