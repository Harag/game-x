(defpackage :game-x-example
  (:use :cl :game-x))

(in-package :game-x-example)

(defclass mygame (game)
  ())


(defparameter draw-h-only-once nil)


(defparameter *position* (list 0 0))

(defun move-down (game &optional (distance 2))  
  (sdl2:render-draw-line (renderer game)  
				  (first *position*) (second *position*)
				  (first *position*)
				  (+ (second *position*) distance))
  (setf *position* (list (first *position*)
				  (+ (second *position*) distance))))

(defun move-right (game &optional (distance 2))
  (sdl2:render-draw-line (renderer game)  
				  (first *position*) (second *position*)
				  (+ (first *position*) distance)
				  (second *position*))
  (setf *position* (list (+ (first *position*) distance)
				  (second *position*))))

(defun move-left (game &optional (distance 2))
  (sdl2:render-draw-line (renderer game)  
				(first *position*) (second *position*)
				(- (first *position*) distance)
				(second *position*))
  (setf *position* (list (- (first *position*) distance)
				(second *position*)))
  )

(defun move-up (game &optional (distance 2))
  (sdl2:render-draw-line (renderer game)  
				(first *position*) (second *position*)
				(first *position*)
				(- (second *position*) distance))
  (setf *position* (list (first *position*)
				(- (second *position*) distance)))
  )

(defun move (game direction &optional (distance 2))
  (cond ((= direction 81)
	 (move-down game distance))
	((= direction 79)
	 (move-right game distance))
	((= direction 82)
	 (move-up game distance))
	((= direction 80)
	 (move-left game distance))))


(defmethod event-handler ((game mygame) event (event-type (eql :key-down)))
;;  (game-x:set-render-draw-color (renderer game) :blue)
;;  (sdl2:render-clear (renderer game))
 ;; (break "~A" event)
  
  (game-x:set-render-draw-color (renderer game) :red)
  
  (move game (scan-code event))
  
  (sdl2:render-present (renderer game)))


(defmethod event-handler ((game mygame) event 
			  (event-type (eql :mouse-motion)))
  

  #|
;;  (game-x:set-render-draw-color (renderer game) :black)
;;  (sdl2:render-clear (renderer game))
  (game-x:set-render-draw-color (renderer game) :green)
  

  (sdl2:render-draw-line (renderer game)  (x event)  20 20 100)
  (sdl2:render-draw-line (renderer game) (x event) 60 60 60)
  (sdl2:render-draw-line (renderer game) (x event) 20 60 100)
  (sdl2:render-present (renderer game))
|#
  )



(defmethod event-handler ((game mygame) event 
			  (event-type (eql :idle)))
  
 
  (unless draw-h-only-once
    
    (game-x:set-render-draw-color (renderer game) :black)
    (sdl2:render-clear (renderer game))
    (game-x:set-render-draw-color (renderer game) :red)
 ;;   (sdl2:render-draw-line (renderer game)  20 20 20 100)
 ;   (sdl2:render-draw-line (renderer game) 20 60 60 60)
 ;   (sdl2:render-draw-line (renderer game) 60 20 60 100)
    (sdl2:render-present (renderer game))
  
    (setf draw-h-only-once t)
    
    
    ))

  

(defun playme ()
  (setf draw-h-only-once nil)
  (setf *position* (list 10 10))
  (let ((game (make-instance 'mygame)))
    (play game)))


