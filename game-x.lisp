(in-package :game-x)

;;(ql:quickload 'sdl2)

(defclass game ()
  ((title :accessor title
	  :initform nil
	  )
   (window :accessor window)
   (renderer :accessor renderer)))

(defclass key-event ()
  ((key-sym :initarg :key-sym :accessor key-sym)
   (scan-code :initarg :scan-code :accessor scan-code)
   (sym-value :initarg :sym-value :accessor sym-value)
   (mod-value :initarg :mod-value :accessor mod-value)))

(defclass mouse-event ()
  ((x :initarg :x :accessor x)
   (y :initarg :y :accessor y)
   (xrel :initarg :xrel :accessor xrel)
   (yrel :initarg :yrel :accessor yrel)
   (state :initarg :state :accessor state)))


;;(defgeneric event-handler (game))

(defmethod event-handler ((game game) (key-event key-event) 
			  (event-type (eql :key-down))))

(defmethod event-handler ((game game) (key-event key-event) 
			  (event-type (eql :key-up))))

(defmethod event-handler ((game game) (mouse-event mouse-event) 
			  (event-type (eql :mouse-motion))))

(defmethod event-handler ((game game) event 
			  (event-type (eql :idle))))

(defun make-key-event (keysym)
  (make-instance 'key-event 
		 :key-sym keysym
		 :scan-code (sdl2:scancode-value keysym)
		 :sym-value (sdl2:sym-value keysym)
		 :mod-value (sdl2:mod-value keysym)))

(defgeneric handle-events (game))

(defmethod handle-events ((game game))
  (sdl2:with-event-loop (:method :poll)
    (:keydown
     (:keysym keysym)
     (event-handler
      game
      (make-key-event keysym)
      :key-down))	    
    (:keyup
     (:keysym keysym)
     (event-handler
      game
      (make-key-event keysym)
      :key-up))
    (:mousemotion
     (:x x :y y :xrel xrel :yrel yrel :state state)
     (event-handler 
      game
      (make-instance 'mouse-event :x x :y y 
		     :xrel xrel :yrel yrel 
		     :state state)
      :mouse-motion))
    (:idle
     ()
     (event-handler game nil :idle))
    (:quit () t)))


(defun set-render-draw-color (renderer color &key (alpha 255))
   (cond ((equal color :black)
	 
	 (sdl2:set-render-draw-color renderer 0 0 0 alpha))
	((equal color :white)
	 (sdl2:set-render-draw-color renderer 255 255 255 alpha))
	((equal color :red)
	 (sdl2:set-render-draw-color renderer 255 0 0 alpha))
	((equal color :lime)
	 (sdl2:set-render-draw-color renderer 0 255 0 alpha))
	((equal color :blue)
	 (sdl2:set-render-draw-color renderer 0 0 255 alpha))
	((equal color :yellow)
	 (sdl2:set-render-draw-color renderer 255 255 0 alpha))
	((equal color :cyan)
	 (sdl2:set-render-draw-color renderer 0 255 255 alpha))
	((equal color :magenta)
	 (sdl2:set-render-draw-color renderer 255 0 255 alpha))
	((equal color :silver)
	 (sdl2:set-render-draw-color renderer 192 192 192 alpha))
	((equal color :gray)
	 (sdl2:set-render-draw-color renderer 128 128 128 alpha))
	((equal color :maroon)
	 (sdl2:set-render-draw-color renderer 128 0 0 alpha))
	((equal color :olive)
	 (sdl2:set-render-draw-color renderer 128 128 0 alpha))
	((equal color :green)
	 (sdl2:set-render-draw-color renderer 0 128 0 alpha))
	((equal color :purple)
	 (sdl2:set-render-draw-color renderer 128 0 128 alpha))
	((equal color :teal)
	 (sdl2:set-render-draw-color renderer 0 128 128 alpha))
	((equal color :navy)
	 (sdl2:set-render-draw-color renderer 0 0 128 alpha))
	(t ;;black
	 (sdl2:set-render-draw-color renderer 0 0 0 alpha))))

(defgeneric play (game))

(defmethod play ((game game))
  (sdl2:with-init (:everything)
    (sdl2:with-window (win :title (title game) :flags '(:shown))
	(setf (window game) win)
	(sdl2:with-renderer (renderer win :flags '(:renderer-accelerated))
	  (setf (renderer game) renderer)
	  (handle-events game)))))
