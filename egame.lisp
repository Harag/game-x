(defpackage :game-x-example
  (:use :cl :game-x))

(in-package :game-x-example)

(defclass et (game)
  ())


(defmethod event-handler ((game et) event (event-type (eql :key-down)))
;;  (game-x:set-render-draw-color (renderer game) :blue)
;;  (sdl2:render-clear (renderer game))
 ;; (break "~A" event)
  
  (game-x:set-render-draw-color (renderer game) :red)
  
  (cond ((= (scan-code event) 81)
	 (sdl2:render-draw-line (renderer game)  10 10 10 200))
	((= (scan-code event) 79)
	 (sdl2:render-draw-line (renderer game)  10 200 200 200))
	((= (scan-code event) 82)
	 (sdl2:render-draw-line (renderer game)  200 200  200 10))
	((= (scan-code event) 80)
	 (sdl2:render-draw-line (renderer game)  200 10 10 10)) 
	)
  
  (sdl2:render-present (renderer game)))

(defparameter once-p nil)

(defmethod event-handler ((game et) event (event-type (eql :idle)))
  (unless once-p
    (game-x:set-render-draw-color (renderer game) :blue)
    (sdl2:render-clear (renderer game))
    (sdl2:render-present (renderer game))
    (setf once-p t)
    )
  )

(defun playme ()
  
  (setf once-p nil)
  (let ((game (make-instance 'et)))
    (play game)
    ))
